import socket
from typing import List, Dict, Tuple
from socketserver import ThreadingTCPServer, BaseRequestHandler

from threading import Thread
from concurrent.futures import ThreadPoolExecutor

class StructData:
    def __init__(self):
        self.uri = None
        self.data = None

        self.disconnect_activily = False

_CallbackRet = Tuple[List[socket.socket], StructData]

def CallbackFunc(request:socket.socket, st:StructData) -> _CallbackRet:
    pass

class Parser:

    def __init__(self):
        pass

    def pre_parse(self, data:bytes) -> Tuple[bytes, bytes]:
        return [data, b""]

    def parse(self, data:bytes) -> StructData:
        return StructData()

class Constructor:

    def __init__(self):
        pass

    def construct(self, data:StructData) -> bytes:
        return b""

class Callbacker:
    def __init__(self):
        pass

    def connected(self, sock:socket.socket, addr:Tuple[str, int]) -> None:
        pass

    def disconnected(self, sock:socket.socket, addr:Tuple[str, int]) -> None:
        pass

    def regiester(self, uri:str, notes:str, fn:CallbackFunc) -> None:
        pass

class WrapperManger:
    def __init__(self, parser:Parser=None, constructor:Constructor=None, callbacker:Callbacker=None):
        self.commends:Dict[str, List[CallbackFunc, CallbackFunc, str]] = dict()
        '''
        self.commends:Dict[uri:str, List[CallbackFunc, note:str]]
        '''

        self.clients:Dict[Tuple[str, int], socket.socket] = dict()
        '''
         : Tuple(ip:str, port:int)
        '''

        self.parser = parser if parser != None else Parser()
        self.constructor = constructor if constructor != None else Constructor()
        self.callbacker = callbacker if callbacker != None else Callbacker()

        self.thread_pool = ThreadPoolExecutor(10)

        self.__stop_request = False

    def connected(self, sock:socket.socket, addr:Tuple[str, int]):
        '''
        已连接, 增加fd
        '''
        if addr not in self.clients:
            self.clients[addr] = sock
            self.callbacker.connected(sock, addr)

    def disconnected(self, sock:socket.socket):
        '''
        已断开连接, 删除fd
        '''
        for addr, _sock in self.clients.items():
            if _sock == sock:
                self.callbacker.disconnected(sock, addr)
                del self.clients[addr]
                break
    
    def stop(self):
        self.__stop_request = True

    def is_stopping(self) -> bool:
        return self.__stop_request

    def register(self, uri:str, notes:str):
        '''
        注册处理函数
        '''
        def decorator(fn:CallbackFunc):
            if uri not in self.commends:
                self.commends[uri] = [fn, notes]
                self.callbacker.regiester(uri, notes, fn)

            def inner(*args, **kwargs):
                ret = fn(*args, **kwargs)
                return ret
            return inner
        return decorator

    def get(self, st:StructData) -> CallbackFunc:
        '''
        获取处理函数
        '''
        fn, notes = self.commends.get(st.uri, [None, ""])
        return fn
    
    def pre_parse(self, data:bytes) -> Tuple[bytes, bytes]:
        return self.parser.pre_parse(data)

    def parse(self, data:bytes) -> StructData:
        '''
        解析数据
        '''
        return self.parser.parse(data)

    def construct(self, st:StructData) -> bytes:
        '''
        构造数据
        '''
        return self.constructor.construct(st)
    
    def _sendall(self, sock:socket.socket, data:bytes):
        sock.sendall(data)

    def send(self, socks:List[socket.socket], data:bytes):
        if socks == None or len(socks) == 0:
            socks = self.clients.values()
        for sock in socks:
            self.thread_pool.submit(self._sendall, sock, data)

class WrapperRequestHandler(BaseRequestHandler):
    def setup(self) -> None:
        self.request.settimeout(5)
        self.manager:WrapperManger = self.server.manager
        '''
        self.manager: WrapperManager
        '''

    def handle(self):
        data:bytes=b''
        while not self.manager.is_stopping():
            try:
                data += self.request.recv(1024)
            except socket.timeout:
                continue
            except OSError as e:
                break

            if len(data) == 0:
                break

            pre_req, data = self.manager.pre_parse(data)
            req_struct = self.manager.parse(pre_req)
            
            # 如果客户端主动断开连接, 则退出循环
            if req_struct.disconnect_activily:
                break

            fn = self.manager.get(req_struct)
            if fn== None:
                continue
            resp_socks, resp_struct = fn(request=self.request, st=req_struct)
            resp = self.manager.construct(resp_struct)
            # 发送/广播
            self.manager.send(resp_socks, resp)

class WrapperTcpServer(ThreadingTCPServer):

    block_on_close = True

    allow_reuse_address = True

    def __init__(self, ip:str, port:int, manager:WrapperManger) -> None:
        ThreadingTCPServer.__init__(self, (ip, port), WrapperRequestHandler)

        self.thread=Thread(target=self.serve_forever_wrapper)

        self.manager=manager

    def serve_forever_wrapper(self):
        try:
            self.serve_forever()
        except Exception as e:
            print(e)

    def get_request(self):
        request, client_address = self.socket.accept()
        self.manager.connected(request, client_address)

        return (request, client_address)

    def close_request(self, request):
        self.manager.disconnected(request)
        request.close()

    def start(self):
        self.thread.start()

    def stop(self):

        # 设置停止标志位，退出 client threads
        # 关闭所有客户端对端socket
        self.manager.stop()

        # 1. 关闭服务端本端socket
        # 2. 等待 client threads 全部退出
        self.server_close()

        # 由于 server_forever() 是跑在线程内的，所以当需要关闭时，shutdown 来停止线程
        self.shutdown()
        self.thread.join()
