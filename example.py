
from example.mydefine import manager
from tcpserver_wrapper import WrapperTcpServer, _CallbackRet

@manager.register("", "回车")
def func1(request, st) -> _CallbackRet:
    print("func1 called")
    return ([request], st)

@manager.register("test", "测试")
def func2(request, st) -> _CallbackRet:
    print("func2 called")
    return ([request], st)

@manager.register("error", "错误")
def func3(request, st) -> _CallbackRet:
    print("func3 called")
    return ([request], st)

@manager.register("func4", "func4")
def func4(request, st) -> _CallbackRet:
    print("func3 called")
    return ([request], st)

server = WrapperTcpServer("127.0.0.1", 1234, manager)
server.start()

while(True):
    pass