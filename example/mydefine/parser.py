from .structdata import MyStructData
from tcpserver_wrapper import Parser

class MyParser(Parser):
    def __init__(self):
        super().__init__()
    
    def parse(self, data:bytes) -> MyStructData:
        message = ""
        try:
            message = data.decode().strip()
        except UnicodeDecodeError:
            return MyStructData("error", None)

        # 客户端主动断开连接
        if message == "bye":
            tmp = MyStructData("", None)
            tmp.disconnect_activily = True
            return tmp

        va_list = message.split(" ")
        if len(va_list) == 0:
            return MyStructData("error", None)
        
        uri = va_list.pop(0)
        # 第一个作为命令，之后的作为参数
        return MyStructData(uri, va_list)


