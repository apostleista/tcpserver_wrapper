from .parser import MyParser
from .constructor import MyConstructor
from tcpserver_wrapper import WrapperManger

parser = MyParser()
constructor = MyConstructor()
manager = WrapperManger(parser, constructor)