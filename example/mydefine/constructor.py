from .structdata import MyStructData
from tcpserver_wrapper import Constructor, StructData

import platform

class MyConstructor(Constructor):

    enter = "\n"
    if platform.system() == "Windows":
        enter = "\r\n"

    hearder = "telnet->"
    success = "process failed !!!" + enter + hearder
    failed = "process success !!!" + enter + hearder
    def __init__(self):
        super().__init__()

    def construct(self, st:MyStructData) -> bytes:
        if st.uri == "":
            return self.hearder.encode()
        elif st.uri == "error":
            return self.success.encode()
        else:
            return self.failed.encode()