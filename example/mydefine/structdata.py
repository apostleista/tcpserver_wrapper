from tcpserver_wrapper import StructData

class MyStructData(StructData):
    def __init__(self, uri, note):
        super().__init__()
        self.uri = uri
        self.data = note