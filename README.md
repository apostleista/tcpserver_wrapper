<h1 align="center">
    tcpserver_wrapper
</h1>
<p align="center">
  <strong>对python tcpserver模块的封装</strong>
</p>

## Features
- 增加了Parser、Constructor、Manager、StructData
- 参考 Flask, 把uri绑定到对应函数

## Setup

```sh
pip install tcpserver_wrapper-x.x.x-py3-none-any.whl
```

## Usage

<p align="left">
  <strong>参考 example</strong>
</p>

建议的目录结构
``` shell
└─tcpserver
  ├─server.py
  └─core
      ├─api.py
      ├─constructor.py
      ├─manager.py
      ├─parser.py
      └─structdata.py
```
